package zenithbank_scanpay.services;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Properties;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;



import zenithbank_scanpay.Dao.AuthDao;
import zenithbank_scanpay.Dao.GenerateQrDao;
import zenithbank_scanpay.entity.AuthEntity;
import zenithbank_scanpay.entity.GenerateQrEntity;
import zenithbank_scanpay.exceptions.BusinessException;
import zenithbank_scanpay.model.response.CallbackResponse;
import zenithbank_scanpay.utility.Global;
import zenithbank_scanpay.utility.PropertyManager;
import zenithbank_scanpay.utility.Utility;

public class RouterTest {

	PropertyManager propertyManager;
	Properties properties;
	
	public RouterTest() {
		propertyManager = new PropertyManager();
		properties = propertyManager.getProperties();
	}
	
public int routeToPostRequest(String payload,String subBasePath, String path )   {
    	
    	try {
    		StringBuilder sb = new StringBuilder();
    		sb.append("{");
    		sb.append("\"passcode\":\"00\",");
    		sb.append("\"message\":\"Successful\"");
    		sb.append("}");
    		return Response.Status.OK.getStatusCode();
    	}catch(Exception ex) {
    		throw new BusinessException(ex);
    	}
    }
    
    public int routeToGetMerchantCategoryCodes(String type, String text, int skip, int limit)  {
    	
    	try {
    		StringBuilder sb = new StringBuilder();
    		sb.append("{");
    		sb.append("\"passcode\":\"00\",");
    		sb.append("\"message\":\"Successful\"");
    		sb.append("}");
    		if(limit == 0) return Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
    		return Response.Status.OK.getStatusCode();
    	}catch(Exception ex) {
    		throw new BusinessException(ex);
    	}
    }
    
    public int routeFetchMerchantIds(String accountNumber) {
    	
    	try {
    		StringBuilder sb = new StringBuilder();
    		sb.append("{");
    		sb.append("\"passcode\":\"00\",");
    		sb.append("\"message\":\"Successful\"");
    		sb.append("}");
    		if(Utility.isNullOrEmpty(accountNumber)) return Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
    		return Response.Status.OK.getStatusCode();
    	}catch(Exception ex) {
    		throw new BusinessException(ex);
    	}
    }
    
    public Response routeToGetCitiesForState(String state) {
    	
    	try {
    		StringBuilder sb = new StringBuilder();
    		sb.append("{");
    		sb.append("\"passcode\":\"00\",");
    		sb.append("\"message\":\"Successful\"");
    		sb.append("}");
    		return Response.status(Status.OK).entity(sb.toString()).type(MediaType.APPLICATION_JSON).build();
    	}catch(Exception ex) {
    		throw new BusinessException(ex);
    	}
    }
    
    public Response processCallback(String callerId, String passcode, CallbackResponse response) {
    	
    	try(GenerateQrDao dboperations = new GenerateQrDao()) {
    		if(!callerId.equals(properties.getProperty(Global.USER_ID)) || !passcode.equals(properties.getProperty(Global.PASS_CODE))) {
    			
    			//logger.info("calledId and Passcode mismatch from callback");
    			throw new BusinessException(new Exception("calledId and Passcode mismatch"));
    		}
    		if(response == null) {
    			//logger.info("Empty payload from callback");
    			throw new BusinessException(new Exception("Empty payload from callback"));
    		}
    		GenerateQrEntity entity = new GenerateQrEntity();
    		
    		entity.setReferenceNumber(response.getReferenceNumber());
    		entity.setReferenceId(response.getReferenceNumber());
			entity.setPaymentDate(response.getPaymentDate());
			entity.setPaymentGateway(response.getPaymentGateway());
			entity.setTransactionCurrency(response.getTransactionCurrency());
			entity.setTransactionReference(response.getTransactionReference());
			entity.setReturnedMerchantId(response.getRetrievalReferenceNumber());
			entity.setConsumerId(response.getConsumerId());
			entity.setBillerId(response.getBillerId());
			entity.setStoreId(response.getStoreId());
			entity.setPhoneNumber(response.getPhoneNumber());
			entity.setRetrievalReferenceNumber(response.getRetrievalReferenceNumber());
			entity.setTerminalId(response.getTerminalId());
			entity.setLoyalityNumber(response.getLoyalityNumber());
			entity.setEmail(response.getEmail());
			entity.setPurchaseIdentifier(response.getPurchaseIdentifier());
			entity.setSecondaryId(response.getSecondaryId());
			
			dboperations.update(entity);
			//dboperations.close();
    		return Response.ok().build();
    	}catch(Exception ex) {
    		//logger.error(ex);
    		throw new BusinessException(ex);
    	}
    	
    }
    
    public Response checkTransactionStatus(String referenceId) {
    	if(Utility.isNullOrEmpty(referenceId)){
    		return Response.status(Status.BAD_GATEWAY).build();
    	}
    	CallbackResponse output = new CallbackResponse();
    	try(GenerateQrDao dboperations = new GenerateQrDao()){
    		GenerateQrEntity response = dboperations.findByReference(referenceId);
        	if(response == null) {
        		return Response.noContent().build();
        	}
        	
        	output.setReferenceNumber(response.getReferenceId());
        	output.setPaymentDate(response.getPaymentDate());
        	output.setPaymentGateway(response.getPaymentGateway());
        	output.setTransactionCurrency(response.getTransactionCurrency());
        	output.setTransactionReference(response.getTransactionReference());
        	output.setReturnedMerchantId(response.getRetrievalReferenceNumber());
        	output.setConsumerId(response.getConsumerId());
        	output.setBillerId(response.getBillerId());
        	output.setStoreId(response.getStoreId());
        	output.setPhoneNumber(response.getPhoneNumber());
        	output.setRetrievalReferenceNumber(response.getRetrievalReferenceNumber());
        	output.setTerminalId(response.getTerminalId());
        	output.setLoyalityNumber(response.getLoyalityNumber());
        	output.setEmail(response.getEmail());
        	output.setPurchaseIdentifier(response.getPurchaseIdentifier());
        	output.setSecondaryId(response.getSecondaryId());
        	output.setPurpose(response.getPurpose());
        	output.setAmount(new BigDecimal(response.getTransactionAmount()));
        	return Response.status(Status.OK).entity(output).build();
    	}catch(Exception ex) {
    		//logger.error(ex);
    		throw new BusinessException(ex);
    	}
    }
    
    public Response createToken(String usercode, String passcode) {
    	String userId = usercode.trim();
    	String passCode = passcode.trim();
    	LocalDateTime dateTime = LocalDateTime.now();
    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
    	String formattedDateTime = dateTime.format(formatter);
    	String plainToken = userId+passCode+formattedDateTime;
    	String token = Utility.encryptThisString(plainToken);
    	
    	AuthEntity entity = new AuthEntity();
    	entity.setUserCode(userId);
    	entity.setPassCode(passCode);
    	entity.setCreatedDate(java.sql.Timestamp.valueOf(dateTime));
    	entity.setToken(token);
    	try(AuthDao authDao = new AuthDao()){
    		authDao.create(entity);
    		StringBuilder sb = new StringBuilder();
    		sb.append("{");
    		sb.append("\"passcode\":\""+passCode+"\",");
    		sb.append("\"message\":\""+token+"\"");
    		sb.append("}");
    		//return Response.status(404).build();
    		return Response.status(Status.OK).entity(sb.toString()).type(MediaType.APPLICATION_JSON).build();
    	}catch(Exception ex) {
    		//logger.error(ex);
    		throw new BusinessException(ex);
    	}
    }
    
    public boolean validateToken(String token, String passcode) {
    	boolean output;
    	try(AuthDao authDao = new AuthDao()){
    		output = authDao.findByTokenAndPasscode(token.trim(), passcode.trim()) != null ? true : false;
    		return output;
    	}catch(Exception ex) {
    		//logger.error(ex);
    		throw new BusinessException(ex);
    	}
    }
}
