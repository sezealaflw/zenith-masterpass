package zenithbank_scanpay.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


import java.sql.Date;
import java.time.LocalDate;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.core.Response;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.mockito.junit.MockitoJUnitRunner;

import zenithbank_scanpay.model.request.FetchTransactionRequest;
import zenithbank_scanpay.model.request.GenerateMerchantIdRequest;
import zenithbank_scanpay.model.request.GenerateQRCodeRequest;
import zenithbank_scanpay.services.RouterTest;
import zenithbank_scanpay.utility.Global;
import zenithbank_scanpay.utility.Utility;

@RunWith(MockitoJUnitRunner.class)
public class ScanPayApiCallsTest {

	
	  String token = "e22077da0f7e40da1bf3b80950a4a8027b7379c16c3107cc0f8b82ee3a547254064512ae3472b077db45ce4529f5ce771cf3cfc51b6badff79fdad1a10f09a4b";
	  String passcode ="samuel";
	public ScanPayApiCallsTest() {
		
	}
	
	@BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
	
	@Test
	public void testGenerateMerchantId (){
		  System.out.println("generateMerchantId");
		  GenerateMerchantIdRequest merchant = new GenerateMerchantIdRequest();
		  merchant.setGateway("mVisa");
		  merchant.setCreatedBy("Chinedu");
		  merchant.setCityName("AWE");
		  merchant.setTradingName("Flutterwave Test Merch");
		  merchant.setRegionName("NASARAWA STATE");
		  merchant.setMerchantCategoryCode("7299");
		  merchant.setMerchantCategoryDescription("Other General Services");
		  merchant.setAcctNumber("1130087141");
		  merchant.setEmail("test@test.com");
		  merchant.setMerchantName("Flutterwave");
		  merchant.setBvn("22142132355");
		 
		  
		
		  
		  try {
			  RouterTest router = new RouterTest();
			  //Mockito.when(router.validateToken(token, passcode)).thenReturn(validate);
			  //assertEquals(true, validate);
			  String payload = merchant.toString();
			  
				int response = router.routeToPostRequest(payload,Global.FINTECH_BASE_PATH,Global.GENERATE_MERCHANT_ID);
				assertNotNull(response);
				

		        assertEquals(Response.Status.OK.getStatusCode(), response);
		  }catch (Exception ex) {
	            Logger.getLogger(ScanPayApiCallsTest.class.getName()).log(Level.SEVERE, null, ex);
	        }
		  
	}
	
	@Test
	public void testGetMerchantCategoryCode() {
		String type = "common";
		String text = "furniture";
		int skip = 0;
		int limit = 10;
		RouterTest router = new RouterTest();
		int response  = router.routeToGetMerchantCategoryCodes(type,text,skip,limit);
		assertNotNull(response);
		
        assertEquals(Response.Status.OK.getStatusCode(), response);
	}
	
	@Test
	public void testGetMerchantCategoryCode_WithoutLimit() {
		String type = "common";
		String text = "furniture";
		int skip = 0;
		int limit = 0;
		RouterTest router = new RouterTest();
		int response  = router.routeToGetMerchantCategoryCodes(type,text,skip,limit);
		assertNotNull(response);	
        assertEquals(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), response);
	}
	@Test
	public void textFetchMerchantId_WithoutAccount() {
		String accountNumber = null;
		RouterTest router = new RouterTest();
		int response  = router.routeFetchMerchantIds(accountNumber);
		assertNotNull(response);	
        assertEquals(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), response);
	}
	@Test
	public void textFetchMerchantId() {
		String accountNumber = "2343671234";
		RouterTest router = new RouterTest();
		int response  = router.routeFetchMerchantIds(accountNumber);
		assertNotNull(response);	
        assertEquals(Response.Status.OK.getStatusCode(), response);
	}
	@Test
	public void testFetchTransactions() {
		FetchTransactionRequest merchant = new FetchTransactionRequest();
		merchant.setStartDate(Date.valueOf(LocalDate.now()));
		merchant.setEndDate(Date.valueOf(LocalDate.now()));
		merchant.setAmount(0);
		merchant.setSkip(0);
		merchant.setLimit(0);
		merchant.setMerchantId("4131833875821348");
		RouterTest router = new RouterTest(); 
		String payload = merchant.toString();  
		int response = router.routeToPostRequest(payload,Global.FINTECH_BASE_PATH,Global.GENERATE_MERCHANT_ID);
		assertNotNull(response);
        assertEquals(Response.Status.OK.getStatusCode(), response);
	}
	@Test
	public void testFetchTransactions_withoutMerhantId() {
		FetchTransactionRequest merchant = new FetchTransactionRequest();
		merchant.setStartDate(Date.valueOf(LocalDate.now()));
		merchant.setEndDate(Date.valueOf(LocalDate.now()));
		merchant.setAmount(0);
		merchant.setSkip(0);
		merchant.setLimit(0);
		
		int response = 0;
		if(Utility.isNullOrEmpty(merchant.getMerchantId()))
			response = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
		
        assertEquals(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), response);
	}
	@Test
	public void testGenerateQRCode() {
		GenerateQRCodeRequest merchant = new GenerateQRCodeRequest();
		merchant.setmVisaMerchantId("4131831506956079");
		merchant.setTransactionAmount("501");
		merchant.setReferenceId("544125492252");
		merchant.setPurpose("Test QR");
		RouterTest router = new RouterTest(); 
		String payload = merchant.toString();  
		int response = router.routeToPostRequest(payload,Global.FINTECH_BASE_PATH,Global.GENERATE_MERCHANT_ID);
		assertNotNull(response);
        assertEquals(Response.Status.OK.getStatusCode(), response);
	}
	
	@Test
	public void testGenerateQRCode_withoutReferenceId() {
		GenerateQRCodeRequest merchant = new GenerateQRCodeRequest();
		merchant.setmVisaMerchantId("4131831506956079");
		merchant.setTransactionAmount("501");
		merchant.setPurpose("Test QR");
		int response = 0;
		if(Utility.isNullOrEmpty(merchant.getReferenceId()))
			response = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
		
        assertEquals(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), response);
	}

}
