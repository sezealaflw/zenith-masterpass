package zenithbank_scanpay.application.config;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import zenithbank_scanpay.controller.ScanPayApiCalls;
import zenithbank_scanpay.controller.Callback;
import zenithbank_scanpay.exceptions.BusinessExceptionMapper;

@ApplicationPath("api")
public class ApplicationConfig extends Application{

	Set<Class<?>> resources = new HashSet<>();
	
	public ApplicationConfig() {
		resources.add(BusinessExceptionMapper.class);
		resources.add(ScanPayApiCalls.class);
		resources.add(Callback.class);
	}
	
	@Override
	public Set<Class<?>> getClasses() {
		// TODO Auto-generated method stub
		return resources;	
	}
	
	

}
