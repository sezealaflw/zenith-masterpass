package zenithbank_scanpay.services;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import zenithbank_scanpay.Dao.AuthDao;
import zenithbank_scanpay.Dao.GenerateQrDao;
import zenithbank_scanpay.entity.AuthEntity;
import zenithbank_scanpay.entity.GenerateQrEntity;
import zenithbank_scanpay.exceptions.BusinessException;
import zenithbank_scanpay.model.response.CallbackResponse;
import zenithbank_scanpay.utility.Global;
import zenithbank_scanpay.utility.PropertyManager;
import zenithbank_scanpay.utility.Utility;

@Stateless
public class Router {
	
	@Inject
	PropertyManager propertyManager;
	
	
	//GenerateQrDao dboperations = new GenerateQrDao();
	
	private WebTarget scanPayApiTarget;
    private Client client;
    
    
    Properties properties;
    
    private static final Logger logger = LogManager.getLogger(Router.class);
    
    
    ObjectMapper objectMapper; 
    
    
    @PostConstruct
    public void initClient() {

    	properties = propertyManager.getProperties();
      ClientBuilder clientBuilder = ClientBuilder.newBuilder()
        .connectTimeout(7, TimeUnit.SECONDS)
        .readTimeout(7, TimeUnit.SECONDS);
        //.register(UserAgentClientFilter.class)
        //.register(ClientLoggingResponseFilter.class);

      this.client = clientBuilder.build();
      this.scanPayApiTarget = client.target(properties.getProperty(Global.QR_SCAN_PAY_URL));
      objectMapper = new ObjectMapper();
      
    }
    
    @PreDestroy
    public void tearDown() {
      this.client.close();
    }
    
    public Response routeToPostRequest(String payload,String subBasePath, String path )   {
    	
    	logger.info("******* Generate MerchantId request payload ********");
    	logger.info(payload);
    	Response postResponse;
    	try {
    		 postResponse = this.scanPayApiTarget.path(properties.getProperty(subBasePath)).path(properties.getProperty(path)).request()
    				.header("CallerID", properties.getProperty(Global.USER_ID))
             		.header("PassCode", properties.getProperty(Global.PASS_CODE))
             		.header("Authorization", "Bearer "+properties.getProperty(Global.TOKEN))
             		.post(Entity.entity(payload, MediaType.APPLICATION_JSON));	
    	}catch(Exception ex) {
    		logger.error(payload, ex);
    		throw new BusinessException(ex);
    	}
    	return postResponse;
    }
    
    public Response routeToGetMerchantCategoryCodes(String type, String text, int skip, int limit)  {
    	
    	Response response;
    	try {
    		String mc = properties.getProperty(Global.FINTECH_BASE_PATH);
    		if(limit == 0) throw new BusinessException(new Exception("Limit cannot be zero(0)"));
    		response = this.scanPayApiTarget.path(properties.getProperty(Global.FINTECH_BASE_PATH))
    				.path(properties.getProperty(Global.GET_MERCHANT_CATEGORY_CODE)).queryParam("type", type)
    				.queryParam("text", text).queryParam("skip", skip).queryParam("limit", limit)
    				.request()
    				.header("CallerID", properties.getProperty(Global.USER_ID))
            		.header("PassCode", properties.getProperty(Global.PASS_CODE))
            		.header("Authorization", "Bearer "+properties.getProperty(Global.TOKEN))
            		.get();
    	}catch(Exception ex) {
    		logger.error(ex);
    		throw new BusinessException(ex);
    	}

		return response;
    }
    
    public Response routeFetchMerchantIds(String accountNumber) {
    	
    	Response response;
    	try {
    		response = this.scanPayApiTarget.path(properties.getProperty(Global.FINTECH_BASE_PATH))
    				.path(properties.getProperty(Global.FETCH_MECHANT_ID)).queryParam("accountNumber", accountNumber)
    				.request()
    				.header("CallerID", properties.getProperty(Global.USER_ID))
            		.header("PassCode", properties.getProperty(Global.PASS_CODE))
            		.header("Authorization", "Bearer "+properties.getProperty(Global.TOKEN))
            		.get();
    	}catch(Exception ex) {
    		logger.error(ex);
    		throw new BusinessException(ex);
    	}

		return response;
    }
    
    public Response routeToGetCitiesForState(String state) {
    	
    	Response response;
    	try {
    		response = this.scanPayApiTarget.path(properties.getProperty(Global.FINTECH_BASE_PATH)).path(properties.getProperty(Global.FETCH_CITIES))
    				.queryParam("State", state)
        			.request()
        			.header("CallerID", properties.getProperty(Global.USER_ID))
            		.header("PassCode", properties.getProperty(Global.PASS_CODE))
            		.header("Authorization", "Bearer "+properties.getProperty(Global.TOKEN))
            		.get();
    	}catch(Exception ex) {
    		logger.error(ex);
    		throw new BusinessException(ex);
    	}
    	
		return response;
    }
    
    public Response processCallback(String callerId, String passcode, CallbackResponse response) {
    	
    	try(GenerateQrDao dboperations = new GenerateQrDao()) {
    		if(!callerId.equals(properties.getProperty(Global.USER_ID)) || !passcode.equals(properties.getProperty(Global.PASS_CODE))) {
    			
    			logger.info("calledId and Passcode mismatch from callback");
    			throw new BusinessException(new Exception("calledId and Passcode mismatch"));
    		}
    		if(response == null) {
    			logger.info("Empty payload from callback");
    			throw new BusinessException(new Exception("Empty payload from callback"));
    		}
    		GenerateQrEntity entity = new GenerateQrEntity();
    		
    		entity.setReferenceNumber(response.getReferenceNumber());
    		entity.setReferenceId(response.getReferenceNumber());
			entity.setPaymentDate(response.getPaymentDate());
			entity.setPaymentGateway(response.getPaymentGateway());
			entity.setTransactionCurrency(response.getTransactionCurrency());
			entity.setTransactionReference(response.getTransactionReference());
			entity.setReturnedMerchantId(response.getRetrievalReferenceNumber());
			entity.setConsumerId(response.getConsumerId());
			entity.setBillerId(response.getBillerId());
			entity.setStoreId(response.getStoreId());
			entity.setPhoneNumber(response.getPhoneNumber());
			entity.setRetrievalReferenceNumber(response.getRetrievalReferenceNumber());
			entity.setTerminalId(response.getTerminalId());
			entity.setLoyalityNumber(response.getLoyalityNumber());
			entity.setEmail(response.getEmail());
			entity.setPurchaseIdentifier(response.getPurchaseIdentifier());
			entity.setSecondaryId(response.getSecondaryId());
			
			dboperations.update(entity);
			//dboperations.close();
    		return Response.ok().build();
    	}catch(Exception ex) {
    		logger.error(ex);
    		throw new BusinessException(ex);
    	}
    	
    }
    
    public Response checkTransactionStatus(String referenceId) {
    	if(Utility.isNullOrEmpty(referenceId)){
    		return Response.status(Status.BAD_GATEWAY).build();
    	}
    	CallbackResponse output = new CallbackResponse();
    	try(GenerateQrDao dboperations = new GenerateQrDao()){
    		GenerateQrEntity response = dboperations.findByReference(referenceId);
        	if(response == null) {
        		return Response.noContent().build();
        	}
        	
        	output.setReferenceNumber(response.getReferenceId());
        	output.setPaymentDate(response.getPaymentDate());
        	output.setPaymentGateway(response.getPaymentGateway());
        	output.setTransactionCurrency(response.getTransactionCurrency());
        	output.setTransactionReference(response.getTransactionReference());
        	output.setReturnedMerchantId(response.getRetrievalReferenceNumber());
        	output.setConsumerId(response.getConsumerId());
        	output.setBillerId(response.getBillerId());
        	output.setStoreId(response.getStoreId());
        	output.setPhoneNumber(response.getPhoneNumber());
        	output.setRetrievalReferenceNumber(response.getRetrievalReferenceNumber());
        	output.setTerminalId(response.getTerminalId());
        	output.setLoyalityNumber(response.getLoyalityNumber());
        	output.setEmail(response.getEmail());
        	output.setPurchaseIdentifier(response.getPurchaseIdentifier());
        	output.setSecondaryId(response.getSecondaryId());
        	output.setPurpose(response.getPurpose());
        	output.setAmount(new BigDecimal(response.getTransactionAmount()));
        	return Response.status(Status.OK).entity(output).build();
    	}catch(Exception ex) {
    		logger.error(ex);
    		throw new BusinessException(ex);
    	}
    }
    
    public Response createToken(String usercode, String passcode) {
    	String userId = usercode.trim();
    	String passCode = passcode.trim();
    	LocalDateTime dateTime = LocalDateTime.now();
    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
    	String formattedDateTime = dateTime.format(formatter);
    	String plainToken = userId+passCode+formattedDateTime;
    	String token = Utility.encryptThisString(plainToken);
    	
    	AuthEntity entity = new AuthEntity();
    	entity.setUserCode(userId);
    	entity.setPassCode(passCode);
    	entity.setCreatedDate(java.sql.Timestamp.valueOf(dateTime));
    	entity.setToken(token);
    	try(AuthDao authDao = new AuthDao()){
    		authDao.create(entity);
    		StringBuilder sb = new StringBuilder();
    		sb.append("{");
    		sb.append("\"passcode\":\""+passCode+"\",");
    		sb.append("\"message\":\""+token+"\"");
    		sb.append("}");
    		//return Response.status(404).build();
    		return Response.status(Status.OK).entity(sb.toString()).type(MediaType.APPLICATION_JSON).build();
    	}catch(Exception ex) {
    		logger.error(ex);
    		throw new BusinessException(ex);
    	}
    }
    
    public boolean validateToken(String token, String passcode) {
    	boolean output;
    	try(AuthDao authDao = new AuthDao()){
    		output = authDao.findByTokenAndPasscode(token.trim(), passcode.trim()) != null ? true : false;
    		return output;
    	}catch(Exception ex) {
    		logger.error(ex);
    		throw new BusinessException(ex);
    	}
    }
	/*
	 * public Response routeToFetchTransactions(String payload) { Response
	 * postResponse = this.scanPayApiTarget.path(Global.FINTECH_BASE_PATH).path(
	 * "/FetchTransactions").request() .header("CallerID", "qrcode_user_test")
	 * .header("PassCode", "qrcode_user_test") .header("Authorization",
	 * "Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE5Mzk5NzI2ODQsIlVzZXIiOiJRckludGVncmF0aW9uQXBwL29iN3BiN3lhdHpoayIsIlR5cGUiOiJwdWJsaWMiLCJEZXZpY2VfaWQiOm51bGwsIkFwcF9pZCI6ImJoemo2YnhueDU3biIsIkFfVXNlcl9pZCI6bnVsbH0.RHgrYUDNlGNpsDYOXbzV0LvGk_INRK00U-sblThmSSDs2ZcakDB7VW0wGc0iwR56uLeBDKpDp3R46JC2LNlgOf91L8Bn567eXIHEvmnoldv-pgi0_WBadAedJEzS3fY94VydKhdgNA3QdKvvNETOg8f-cWugNpk6GlSvFwSmfmNXfC_fisrTgdFit05NoHteWSc1sI8NXtZscqxrySNzrFixUG2dMqqlt1tKBx7ocgEEyPwKSvSeE3jYL92MkWxPhDATswCf9Zr7qf0Hjk-u3fENAzoo_EnFjl4x-QA_qbk06OfzHsfjKgfd4BW3O_UklA0ADz_3ajLOx4p0EjyrUoKyeMusrAIbryJATQR_EdeIUaTYgq1_o6shfTBgaFN9LF8YgEzG4Bg32TgozvzmXcLuIXXjAqVa6wely17oy0JijeOnOyMXB2dJOIZz--r0YS8kL9d2vHVih4sa5tPt9coIf6cdulCF-8OkKqO5uz_xxV1VAbcKRFRXCBAkTWKjmhTJlluizEAIcQQNy2nSw8PQgPSa5ha9xmy81b9Gsw2COIq8bOnnDczdwd7fxhkFRB8pOTyKhG7ly3XvO7kDC01OicC-aAEug0cFSJZ9mML0hZfEli8Mb-_ssMGyInttIDxyPK2kNdnWxFji9XVAlHMxhHF0QuRbaagmQ79otZA"
	 * ) .post(Entity.entity(payload, MediaType.APPLICATION_JSON));
	 * 
	 * return postResponse; }
	 * 
	 * public Response routeToGenerateQRCode(String payload) { Response postResponse
	 * =
	 * this.scanPayApiTarget.path(Global.MERCHANT_BASE_PATH).path("/GenerateQRCode")
	 * .request() .header("CallerID", "qrcode_user_test") .header("PassCode",
	 * "qrcode_user_test") .header("Authorization",
	 * "Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE5Mzk5NzI2ODQsIlVzZXIiOiJRckludGVncmF0aW9uQXBwL29iN3BiN3lhdHpoayIsIlR5cGUiOiJwdWJsaWMiLCJEZXZpY2VfaWQiOm51bGwsIkFwcF9pZCI6ImJoemo2YnhueDU3biIsIkFfVXNlcl9pZCI6bnVsbH0.RHgrYUDNlGNpsDYOXbzV0LvGk_INRK00U-sblThmSSDs2ZcakDB7VW0wGc0iwR56uLeBDKpDp3R46JC2LNlgOf91L8Bn567eXIHEvmnoldv-pgi0_WBadAedJEzS3fY94VydKhdgNA3QdKvvNETOg8f-cWugNpk6GlSvFwSmfmNXfC_fisrTgdFit05NoHteWSc1sI8NXtZscqxrySNzrFixUG2dMqqlt1tKBx7ocgEEyPwKSvSeE3jYL92MkWxPhDATswCf9Zr7qf0Hjk-u3fENAzoo_EnFjl4x-QA_qbk06OfzHsfjKgfd4BW3O_UklA0ADz_3ajLOx4p0EjyrUoKyeMusrAIbryJATQR_EdeIUaTYgq1_o6shfTBgaFN9LF8YgEzG4Bg32TgozvzmXcLuIXXjAqVa6wely17oy0JijeOnOyMXB2dJOIZz--r0YS8kL9d2vHVih4sa5tPt9coIf6cdulCF-8OkKqO5uz_xxV1VAbcKRFRXCBAkTWKjmhTJlluizEAIcQQNy2nSw8PQgPSa5ha9xmy81b9Gsw2COIq8bOnnDczdwd7fxhkFRB8pOTyKhG7ly3XvO7kDC01OicC-aAEug0cFSJZ9mML0hZfEli8Mb-_ssMGyInttIDxyPK2kNdnWxFji9XVAlHMxhHF0QuRbaagmQ79otZA"
	 * ) .post(Entity.entity(payload, MediaType.APPLICATION_JSON));
	 * 
	 * return postResponse; }
	 */

}
