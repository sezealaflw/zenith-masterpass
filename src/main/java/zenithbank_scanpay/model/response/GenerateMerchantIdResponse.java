package zenithbank_scanpay.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GenerateMerchantIdResponse {
	@JsonProperty("merchantId")
    public String merchantId;
    @JsonProperty("merchantPan")
    public String merchantPan;
    @JsonProperty("cityName")
    public String cityName;
    @JsonProperty("responseCode")
    public String responseCode;
    @JsonProperty("responseDescription")
    public String responseDescription;

    @JsonProperty("panExpiryDate")
    public String panExpiryDate;
    @JsonProperty("clientCode")
    public String clientCode;
    @JsonProperty("cvv")
    public String cvv;
    @JsonProperty("requestId")
    public String requestId;
    @JsonProperty("mappingId")
    public String mappingId;
    @JsonProperty("cityCode")
    public String cityCode;
    @JsonProperty("regionCode")
    public String regionCode;

    public String getClientCode() {
        return this.clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getCvv() {
        return this.cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public String getRequestId() {
        return this.requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getMappingId() {
        return this.mappingId;
    }

    public void setMappingId(String mappingId) {
        this.mappingId = mappingId;
    }

    public String getCityCode() {
        return this.cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getRegionCode() {
        return this.regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getPanExpiryDate() {
        return this.panExpiryDate;
    }

    public void setPanExpiryDate(String panExpiryDate) {
        this.panExpiryDate = panExpiryDate;
    }

    public String getResponseCode() {
        return this.responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseDescription() {
        return this.responseDescription;
    }

    public void setResponseDescription(String responseDescription) {
        this.responseDescription = responseDescription;
    }

    public String getMerchantId() {
        return this.merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantPan() {
        return this.merchantPan;
    }

    public void setMerchantPan(String merchantPan) {
        this.merchantPan = merchantPan;
    }

    public String getCityName() {
        return this.cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
}
