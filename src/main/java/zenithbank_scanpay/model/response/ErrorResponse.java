package zenithbank_scanpay.model.response;

public class ErrorResponse {

	private String responseCode;
	private String responseMessage;
	private int status;
	
	public ErrorResponse(String code, String message, int status) {
		this.responseCode = code;
		this.responseMessage = message;
		this.status = status;
	}
	public String getResponseCode() {
		return responseCode;
	}
	
	public String getResponseMessage() {
		return responseMessage;
	}
	
	public int getStatus() {
		return status;
	}
	
}
