package zenithbank_scanpay.model.request;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class GenerateQRCodeRequest {
	private String mVisaMerchantId;
	private String masterPassMerchantId;
	private String storeId;
	private String purpose;
	private String transactionAmount;
	private String tipAndFeeIndicator;
	private String convenienceFeeAmount;
	private String convenienceFeePercentage;
	private boolean isMobileNumberMandatory;
	private String mobileNumber;
	private boolean isStoreIdMandatory;
	private boolean isLoyaltyNumberMandatory;
	private String loyaltyNumber;
	private boolean isReferenceIdMandatory = true;
	@NotBlank(message = "Reference ID must be provided")
	private String referenceId ;
	private boolean isConsumerIdMandatory;
	private String consumerId;
	private boolean isTerminalIdMandatory;
	private String terminalId;
	private boolean isPurposeMandatory;
	private String billId;
	private boolean isBillIdMandatory;

    public String getmVisaMerchantId() {
        return this.mVisaMerchantId;
    }

    public void setmVisaMerchantId(String mVisaMerchantId) {
        this.mVisaMerchantId = mVisaMerchantId;
    }

    public String getMasterPassMerchantId() {
        return this.masterPassMerchantId;
    }

    public void setMasterPassMerchantId(String masterPassMerchantId) {
        this.masterPassMerchantId = masterPassMerchantId;
    }

    public String getStoreId() {
        return this.storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getPurpose() {
        return this.purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getTransactionAmount() {
        return this.transactionAmount;
    }

    public void setTransactionAmount(String transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getTipAndFeeIndicator() {
        return this.tipAndFeeIndicator;
    }

    public void setTipAndFeeIndicator(String tipAndFeeIndicator) {
        this.tipAndFeeIndicator = tipAndFeeIndicator;
    }

    public String getConvenienceFeeAmount() {
        return this.convenienceFeeAmount;
    }

    public void setConvenienceFeeAmount(String convenienceFeeAmount) {
        this.convenienceFeeAmount = convenienceFeeAmount;
    }

    public String getConvenienceFeePercentage() {
        return this.convenienceFeePercentage;
    }

    public void setConvenienceFeePercentage(String convenienceFeePercentage) {
        this.convenienceFeePercentage = convenienceFeePercentage;
    }

    public boolean isIsMobileNumberMandatory() {
        return this.isMobileNumberMandatory;
    }

    public boolean getIsMobileNumberMandatory() {
        return this.isMobileNumberMandatory;
    }

    public void setIsMobileNumberMandatory(boolean isMobileNumberMandatory) {
        this.isMobileNumberMandatory = isMobileNumberMandatory;
    }

    public String getMobileNumber() {
        return this.mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public boolean isIsStoreIdMandatory() {
        return this.isStoreIdMandatory;
    }

    public boolean getIsStoreIdMandatory() {
        return this.isStoreIdMandatory;
    }

    public void setIsStoreIdMandatory(boolean isStoreIdMandatory) {
        this.isStoreIdMandatory = isStoreIdMandatory;
    }

    public boolean isIsLoyaltyNumberMandatory() {
        return this.isLoyaltyNumberMandatory;
    }

    public boolean getIsLoyaltyNumberMandatory() {
        return this.isLoyaltyNumberMandatory;
    }

    public void setIsLoyaltyNumberMandatory(boolean isLoyaltyNumberMandatory) {
        this.isLoyaltyNumberMandatory = isLoyaltyNumberMandatory;
    }

    public String getLoyaltyNumber() {
        return this.loyaltyNumber;
    }

    public void setLoyaltyNumber(String loyaltyNumber) {
        this.loyaltyNumber = loyaltyNumber;
    }

    public boolean isIsReferenceIdMandatory() {
        return this.isReferenceIdMandatory;
    }

    public boolean getIsReferenceIdMandatory() {
        return this.isReferenceIdMandatory;
    }

    public void setIsReferenceIdMandatory(boolean isReferenceIdMandatory) {
        this.isReferenceIdMandatory = isReferenceIdMandatory;
    }

    public String getReferenceId() {
        return this.referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public boolean isIsConsumerIdMandatory() {
        return this.isConsumerIdMandatory;
    }

    public boolean getIsConsumerIdMandatory() {
        return this.isConsumerIdMandatory;
    }

    public void setIsConsumerIdMandatory(boolean isConsumerIdMandatory) {
        this.isConsumerIdMandatory = isConsumerIdMandatory;
    }

    public String getConsumerId() {
        return this.consumerId;
    }

    public void setConsumerId(String consumerId) {
        this.consumerId = consumerId;
    }

    public boolean isIsTerminalIdMandatory() {
        return this.isTerminalIdMandatory;
    }

    public boolean getIsTerminalIdMandatory() {
        return this.isTerminalIdMandatory;
    }

    public void setIsTerminalIdMandatory(boolean isTerminalIdMandatory) {
        this.isTerminalIdMandatory = isTerminalIdMandatory;
    }

    public String getTerminalId() {
        return this.terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public boolean isIsPurposeMandatory() {
        return this.isPurposeMandatory;
    }

    public boolean getIsPurposeMandatory() {
        return this.isPurposeMandatory;
    }

    public void setIsPurposeMandatory(boolean isPurposeMandatory) {
        this.isPurposeMandatory = isPurposeMandatory;
    }

    public String getBillId() {
        return this.billId;
    }

    public void setBillId(String billId) {
        this.billId = billId;
    }

    public boolean isIsBillIdMandatory() {
        return this.isBillIdMandatory;
    }

    public boolean getIsBillIdMandatory() {
        return this.isBillIdMandatory;
    }

    public void setIsBillIdMandatory(boolean isBillIdMandatory) {
        this.isBillIdMandatory = isBillIdMandatory;
    }
    
    @Override
    public String toString() {
        
    	ObjectMapper objectMapper = new ObjectMapper();
    	try {
			return objectMapper.writeValueAsString((this));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
    }
}
