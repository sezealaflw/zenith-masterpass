package zenithbank_scanpay.exceptions;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import zenithbank_scanpay.model.response.ErrorResponse;

@Provider
public class BusinessExceptionMapper implements ExceptionMapper<BusinessException> {

	@Override
	public Response toResponse(BusinessException exception) {
		
		/*
		 * StringBuilder sb = new StringBuilder(); sb.append("{");
		 * sb.append("\"responseCode\":\"99\",");
		 * sb.append("\"responseDescription\":\"An error occurred try again later\"");
		 * sb.append("}");
		 */
		//return Response.status(404).build();
		return Response.status(Status.INTERNAL_SERVER_ERROR)
				.entity( new ErrorResponse("99",exception.getMessage(),500) )
				.type(MediaType.APPLICATION_JSON).build();
	}
}
