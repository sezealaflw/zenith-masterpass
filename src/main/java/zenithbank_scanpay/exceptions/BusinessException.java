package zenithbank_scanpay.exceptions;

public class BusinessException extends RuntimeException {

	
	private static final long serialVersionUID = 1L;
	
	public BusinessException(Exception ex) {
		Message = ex.getMessage();
		Throwable cause = ex.getCause();
		if(cause != null) {
			Message = cause.getMessage();
		}
	}
	
	private String Message ;

	public String getMessage() {
		return Message;
	}

	
}
