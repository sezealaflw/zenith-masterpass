package zenithbank_scanpay.controller;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;



//import java.util.HashMap;
//import java.util.Map;

import javax.inject.Inject;
import javax.validation.Valid;


import zenithbank_scanpay.Dao.GenerateQrDao;
import zenithbank_scanpay.entity.GenerateQrEntity;
import zenithbank_scanpay.exceptions.BusinessException;
import zenithbank_scanpay.model.request.FetchTransactionRequest;
import zenithbank_scanpay.model.request.GenerateMerchantIdRequest;
import zenithbank_scanpay.model.request.GenerateQRCodeRequest;
import zenithbank_scanpay.services.Router;
import zenithbank_scanpay.utility.Global;
import zenithbank_scanpay.utility.Utility;



@Path(value = "v1")
@Produces(value = {MediaType.APPLICATION_JSON})
public class ScanPayApiCalls {
	
	private static final Logger logger = LogManager.getLogger(ScanPayApiCalls.class);
	
	@Inject 
	Router router;
	
	
	
	@POST
	@Path(value="generatemerchantid")
	public Response generateMerchantId(@Context HttpHeaders headers,GenerateMerchantIdRequest merchant) {
		
		String passcode = headers.getHeaderString("X-Passcode");
		String authorization = headers.getHeaderString("Authorization");
		checkNullHeader(passcode, authorization);
		checkForBearer(authorization);
		if(router.validateToken(getToken(authorization,1), passcode))
		{
			String payload = merchant.toString();
			Response response = router.routeToPostRequest(payload,Global.FINTECH_BASE_PATH,Global.GENERATE_MERCHANT_ID);
			return response;
		}
		else
			throw new BusinessException( new Exception("Invalid token supplied"));
		
	}
	
	@GET
	@Path(value="getmerchantcategorycode")
	public Response getMerchantCategoryCode(@Context HttpHeaders headers, @QueryParam(value = "type") String type, @QueryParam(value = "text") String text,
			@QueryParam(value = "skip") int skip, @QueryParam(value = "limit") int limit ) {
		
		String passcode = headers.getHeaderString("X-Passcode");
		String authorization = headers.getHeaderString("Authorization");
		checkNullHeader(passcode, authorization);
		checkForBearer(authorization);
		Response response;
		if(router.validateToken(getToken(authorization,1), passcode))
		{
			response = router.routeToGetMerchantCategoryCodes(type,text,skip,limit);
			return response;
		}
		else
			throw new BusinessException( new Exception("Invalid token supplied"));
	}
	
	@GET
	@Path(value="getcitiesforstate")
	public Response getCitiesForState(@Context HttpHeaders headers, @QueryParam(value = "state") String state) {
		String passcode = headers.getHeaderString("X-Passcode");
		String authorization = headers.getHeaderString("Authorization");
		checkNullHeader(passcode, authorization);
		checkForBearer(authorization);
		Response response;
		if(router.validateToken(getToken(authorization,1), passcode))
		{
			 response = router.routeToGetCitiesForState(state);
			return response;
		}
		else
			throw new BusinessException( new Exception("Invalid token supplied"));
		
	}
	
	@GET
	@Path(value="fetchmerchantid")
	public Response fetchMerchantId(@Context HttpHeaders headers,@QueryParam(value = "accountNumber") String accountNumber) {
		
		String passcode = headers.getHeaderString("X-Passcode");
		String authorization = headers.getHeaderString("Authorization");
		checkNullHeader(passcode, authorization);
		checkForBearer(authorization);
		Response response;
		if(router.validateToken(getToken(authorization,1), passcode))
		{
			response = router.routeFetchMerchantIds(accountNumber);
			return response;
		}
		else
			throw new BusinessException( new Exception("Invalid token supplied")); 
	}
	
	@POST
	@Path(value="fetchtransactions")
	public Response fetchTransactions(@Context HttpHeaders headers, @Valid FetchTransactionRequest merchant) {
		String passcode = headers.getHeaderString("X-Passcode");
		String authorization = headers.getHeaderString("Authorization");
		checkNullHeader(passcode, authorization);
		checkForBearer(authorization);
		Response response;
		if(router.validateToken(getToken(authorization,1), passcode))
		{
			String payload = merchant.toString();
		    response = router.routeToPostRequest(payload,Global.FINTECH_BASE_PATH,Global.FETCH_TRANSACTIONS);
			return response;
		}
		else
			throw new BusinessException( new Exception("Invalid token supplied")); 
	}
	
	@POST
	@Path(value="generateqrcode")
	public Response generateQRCode(@Context HttpHeaders headers, @Valid GenerateQRCodeRequest merchant) {
		
		String passcode = headers.getHeaderString("X-Passcode");
		String authorization = headers.getHeaderString("Authorization");
		checkNullHeader(passcode, authorization);
		checkForBearer(authorization);
		if(!router.validateToken(getToken(authorization,1), passcode))
			throw new BusinessException( new Exception("Invalid token supplied"));
		
		if(Utility.isNullOrEmpty(merchant.getMasterPassMerchantId())
				&& Utility.isNullOrEmpty(merchant.getmVisaMerchantId())) {
			throw new BusinessException( new Exception("Either mVisa Merchant Id or Masterpass Merchant Id or both should be provided"));
		}
		String payload = merchant.toString();
		
		
		GenerateQrEntity entity = new GenerateQrEntity();
		entity.setMerchantId(Utility.isNullOrEmpty(merchant.getMasterPassMerchantId())
				?merchant.getMasterPassMerchantId():merchant.getmVisaMerchantId());
		entity.setTransactionAmount(merchant.getTransactionAmount());
		entity.setReferenceId(merchant.getReferenceId());
		entity.setPurpose(merchant.getPurpose());
		try(GenerateQrDao dboperations = new GenerateQrDao()){
			dboperations.create(entity);
		}catch(Exception ex) {
    		logger.error(payload, ex);
    		throw new BusinessException(ex);
    	}
		
		
		Response response = router.routeToPostRequest(payload,Global.MERCHANT_BASE_PATH,Global.GENERATE_QR);
		return response;
	}
	
	@GET
	@Path(value="status")
	public Response checkStatus(@Context HttpHeaders headers, @QueryParam(value = "reference") String reference) {
		String passcode = headers.getHeaderString("X-Passcode");
		String authorization = headers.getHeaderString("Authorization");
		checkNullHeader(passcode, authorization);
		checkForBearer(authorization);
		if(router.validateToken(getToken(authorization,1), passcode))
			return router.checkTransactionStatus(reference);
		else
			throw new BusinessException( new Exception("Invalid token supplied"));
	}

	
	
	@POST
	@Path(value="getToken")
	public Response GenerateToken(@QueryParam(value="userid") String userId, @QueryParam(value="passcode") String passCode) {
		if(Utility.isNullOrEmpty(userId)
				&& Utility.isNullOrEmpty(passCode)) {
			throw new BusinessException( new Exception("User Id and PassCode is required"));
		}
		Response response = router.createToken(userId,passCode);
		return response;
	}
	
	private void checkNullHeader(String passcode, String authorization) {
		if(Utility.isNullOrEmpty(passcode))
			throw new BusinessException( new Exception("X-Passcode header parameter is required"));
		if(Utility.isNullOrEmpty(authorization))
			throw new BusinessException( new Exception("Authorization is required"));
	}
	
	private  String getToken(String headerKey, int index) {
        String[] array = headerKey.split(" ");
        return array[index].toString();
    }
	
	private void checkForBearer(String headerKey) {
		if(!getToken(headerKey,0).equals("Bearer"))
			throw new BusinessException( new Exception("Pass a Bearer token"));
	}
}
