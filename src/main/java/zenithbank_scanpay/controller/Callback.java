package zenithbank_scanpay.controller;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import zenithbank_scanpay.model.response.CallbackResponse;
import zenithbank_scanpay.services.Router;

@Path(value = "hooks")
@Produces(value = MediaType.APPLICATION_JSON)
@Consumes(value = MediaType.APPLICATION_JSON)
public class Callback {
	
	@Inject 
	Router router;
	

	@POST
	@Path(value="qr")
	public Response processCallBack(@Context  HttpHeaders headers, CallbackResponse response) {
		
		String userId = headers.getHeaderString("X-UserId");
		String userPassCode = headers.getHeaderString("X-Passcode");
		
		Response output = router.processCallback(userId, userPassCode, response);
		return output;
	}
	
}
