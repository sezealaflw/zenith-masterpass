package zenithbank_scanpay.Dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import zenithbank_scanpay.entity.AuthEntity;

public class AuthDao implements AutoCloseable {
	private  EntityManagerFactory ENTITY_MANAGER_FACTORY = Persistence.createEntityManagerFactory("zenithbank_scanpay");
	 private static final Logger logger = LogManager.getLogger(AuthDao.class);
	 
	 public void create(AuthEntity authEntity) {
			EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
			EntityTransaction  et = null;
			
			try {
				et = em.getTransaction();
				et.begin();
				em.persist(authEntity);
				et.commit();
			}
			
			catch(Exception ex) {
				if(et != null) {
					et.rollback();
				}
				logger.error(ex);
			}
			finally {
				em.close();
			}
		}
	 
	 public AuthEntity findByTokenAndPasscode(String key, String passcode) {
			EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
			String query = "SELECT c FROM AuthTbl c WHERE c.token = :key AND c.passCode = :pcode";
			
			TypedQuery<AuthEntity> tq = em.createQuery(query,AuthEntity.class);
			tq.setParameter("key", key);
			tq.setParameter("pcode", passcode);
			AuthEntity qrRequest = null;
			try {
				qrRequest = tq.getSingleResult();
			}
			catch(NoResultException ex) {
				logger.error(ex);
			}
			catch(Exception ex) {
				logger.error(ex);
			}finally {
				em.close();
			}
			return qrRequest;
		}

	@Override
	public void close() throws Exception {
		ENTITY_MANAGER_FACTORY.close();
		
	}
}
