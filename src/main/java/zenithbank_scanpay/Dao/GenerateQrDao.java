package zenithbank_scanpay.Dao;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import zenithbank_scanpay.entity.GenerateQrEntity;


public class GenerateQrDao implements AutoCloseable {

	private  EntityManagerFactory ENTITY_MANAGER_FACTORY = Persistence.createEntityManagerFactory("zenithbank_scanpay");
	 private static final Logger logger = LogManager.getLogger(GenerateQrDao.class);
	
	public void create(GenerateQrEntity qrEntity) {
		EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction  et = null;
		
		try {
			et = em.getTransaction();
			et.begin();
			em.persist(qrEntity);
			et.commit();
		}
		
		catch(Exception ex) {
			if(et != null) {
				et.rollback();
			}
			logger.error(ex);
		}
		finally {
			em.close();
		}
	}
	
	public GenerateQrEntity findById(long id) {
		EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
		String query = "SELECT c FROM ZenithLog c WHERE c.id = :logID";
		
		TypedQuery<GenerateQrEntity> tq = em.createQuery(query,GenerateQrEntity.class);
		tq.setParameter("logID", id);
		GenerateQrEntity qrRequest = null;
		try {
			qrRequest = tq.getSingleResult();
		}
		catch(NoResultException ex) {
			logger.error(ex);
		}
		catch(Exception ex) {
			logger.error(ex);
		}finally {
			em.close();
		}
		return qrRequest;
	}
	
	public GenerateQrEntity findByReference(String reference) {
		EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
		String query = "SELECT c FROM ZenithLog c WHERE c.referenceId = :refID";
		
		TypedQuery<GenerateQrEntity> tq = em.createQuery(query,GenerateQrEntity.class);
		tq.setParameter("refID", reference);
		GenerateQrEntity qrRequest = null;
		try {
			qrRequest = tq.getSingleResult();
		}
		catch(NoResultException ex) {
			logger.error(ex);
		}
		catch(Exception ex) {
			logger.error(ex);
		}finally {
			em.close();
		}
		return qrRequest;
	}
	
	public void update(GenerateQrEntity qrEntity) {
		EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction  et = null;
		
		String query = "SELECT c FROM ZenithLog c WHERE c.referenceId = :refID";
		
		TypedQuery<GenerateQrEntity> tq = em.createQuery(query,GenerateQrEntity.class);
		tq.setParameter("refID", qrEntity.getReferenceId());
		GenerateQrEntity qr = null;
		try {
			et = em.getTransaction();
			et.begin();
			//qr = em.find(GenerateQrEntity.class, qrEntity.getReferenceId());
			qr = tq.getSingleResult();
			if(qr != null) {
				qr.setReferenceNumber(qrEntity.getReferenceNumber());
				qr.setPaymentDate(qrEntity.getPaymentDate());
				qr.setPaymentGateway(qrEntity.getPaymentGateway());
				qr.setTransactionCurrency(qrEntity.getTransactionCurrency());
				qr.setTransactionReference(qrEntity.getTransactionReference());
				qr.setReturnedMerchantId(qrEntity.getRetrievalReferenceNumber());
				qr.setConsumerId(qrEntity.getConsumerId());
				qr.setBillerId(qrEntity.getBillerId());
				qr.setStoreId(qrEntity.getStoreId());
				qr.setPhoneNumber(qrEntity.getPhoneNumber());
				qr.setRetrievalReferenceNumber(qrEntity.getRetrievalReferenceNumber());
				qr.setTerminalId(qrEntity.getTerminalId());
				qr.setLoyalityNumber(qrEntity.getLoyalityNumber());
				qr.setEmail(qrEntity.getEmail());
				qr.setPurchaseIdentifier(qrEntity.getPurchaseIdentifier());
				qr.setSecondaryId(qrEntity.getSecondaryId());
				
				em.persist(qr);
				et.commit();
			}
			
		}
		catch(Exception ex) {
			if(et != null) {
				et.rollback();
			}
			logger.error(ex);
		}
		finally {
			em.close();
		}
		
	}
	
	@Override
	public void close()  throws Exception  {
		ENTITY_MANAGER_FACTORY.close();
	}
}
