package zenithbank_scanpay.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;

@Entity(name ="ZenithLog")
@Table(name = "zenith_bank_qr_log")
public class GenerateQrEntity implements Serializable {

	
	private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "log_id", unique= true)
	private long id;
	@Column(name = "merchant_id")
	private String merchantId;
	@Column(name = "purpose")
	private String purpose;
	@Column(name = "transaction_amount")
	private String transactionAmount;
	@Column(name = "reference_id")
	private String referenceId ;
	@Column(name = "request_date")
	private String requestDate;
	
	@Column(name = "retrieval_reference_number")
	private String retrievalReferenceNumber;
	@Column(name = "payment_date")
	private String paymentDate;
	@Column(name = "payment_gateway")
	private String paymentGateway;
	@Column(name = "transaction_currency")
	private String transactionCurrency;
	@Column(name = "transaction_reference")
	private String transactionReference;
	@Column(name = "returned_merchant_id")
	private String returnedMerchantId;
	@Column(name = "consumer_id")
	private String consumerId;
	@Column(name = "biller_id")
	private String billerId;
	@Column(name = "store_id")
	private String storeId;
	@Column(name = "phone_number")
	private String phoneNumber;
	@Column(name = "reference_number")
	private String referenceNumber;
	@Column(name = "terminal_id")
	private String terminalId;
	@Column(name = "loyality_number")
	private String loyalityNumber;
	@Column(name = "address")
	private String address;
	@Column(name = "email")
	private String email;
	@Column(name = "purchase_identifier")
	private String purchaseIdentifier;
	@Column(name = "secondary_id")
	private String secondaryId;
	
//	@Column(name = "return_date")
//	private String returnDate;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public String getTransactionAmount() {
		return transactionAmount;
	}

	public void setTransactionAmount(String transactionAmount) {
		this.transactionAmount = transactionAmount;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public String getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(String requestDate) {
		this.requestDate = requestDate;
	}

	public String getRetrievalReferenceNumber() {
		return retrievalReferenceNumber;
	}

	public void setRetrievalReferenceNumber(String retrievalReferenceNumber) {
		this.retrievalReferenceNumber = retrievalReferenceNumber;
	}

	public String getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getPaymentGateway() {
		return paymentGateway;
	}

	public void setPaymentGateway(String paymentGateway) {
		this.paymentGateway = paymentGateway;
	}

	public String getTransactionCurrency() {
		return transactionCurrency;
	}

	public void setTransactionCurrency(String transactionCurrency) {
		this.transactionCurrency = transactionCurrency;
	}

	public String getTransactionReference() {
		return transactionReference;
	}

	public void setTransactionReference(String transactionReference) {
		this.transactionReference = transactionReference;
	}

	public String getReturnedMerchantId() {
		return returnedMerchantId;
	}

	public void setReturnedMerchantId(String returnedMerchantId) {
		this.returnedMerchantId = returnedMerchantId;
	}

	public String getConsumerId() {
		return consumerId;
	}

	public void setConsumerId(String consumerId) {
		this.consumerId = consumerId;
	}

	public String getBillerId() {
		return billerId;
	}

	public void setBillerId(String billerId) {
		this.billerId = billerId;
	}

	public String getStoreId() {
		return storeId;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	public String getLoyalityNumber() {
		return loyalityNumber;
	}

	public void setLoyalityNumber(String loyalityNumber) {
		this.loyalityNumber = loyalityNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPurchaseIdentifier() {
		return purchaseIdentifier;
	}

	public void setPurchaseIdentifier(String purchaseIdentifier) {
		this.purchaseIdentifier = purchaseIdentifier;
	}

	public String getSecondaryId() {
		return secondaryId;
	}

	public void setSecondaryId(String secondaryId) {
		this.secondaryId = secondaryId;
	}

//	public String getReturnDate() {
//		return returnDate;
//	}
//
//	public void setReturnDate(String returnDate) {
//		this.returnDate = returnDate;
//	}
	
	
}
