package zenithbank_scanpay.utility;

public class Global {

	public static final String QR_SCAN_PAY_URL = "base_url";
	public static final String FINTECH_BASE_PATH = "fintech_base_path";
	public static final String MERCHANT_BASE_PATH = "merchant_base_path";
	public static final String GET_MERCHANT_CATEGORY_CODE="getmerchant_category_code";
	public static final String FETCH_MECHANT_ID="fetch_merchant_id";
	public static final String FETCH_CITIES = "fetch_cites_for_state";
	public static final String GENERATE_MERCHANT_ID = "generate_merchant_id";
	public static final String FETCH_TRANSACTIONS = "fetch_transactions";
	public static final String GENERATE_QR = "generate_qr_code";
	public static final String USER_ID = "user_id";
	public static final String PASS_CODE ="passcode";
	public static final String TOKEN ="token";
	
}
