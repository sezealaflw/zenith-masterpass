package zenithbank_scanpay.utility;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import javax.ejb.Init;
import javax.ejb.Singleton;

@Singleton
public class PropertyManager {
	
	private Properties properties;
	
	
	
	@Init
    public void setUp(){
		
        properties =  loadFile();
       
    }
	
	private  Properties loadFile() {
		Properties props = new Properties();
		try {
			FileReader reader = new FileReader("//Users/sasmuelezeala/Documents/javaproject-eclipse/properties/qrconfig.properties");
			props.load(reader);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return props;
	}
	
	public String get(String key, String defaultValue){
        
        return getProperties().getProperty(key, defaultValue);
    }
	
	public Properties getProperties(){
        
        if(properties == null)
            properties = loadFile();
        
        return properties;
    }
   

}
